#include <stddef.h>
int gauss_grid_create(size_t size, double* x, double* w, double xmin, double xmax);
int test_gauss_grid_create(void);
