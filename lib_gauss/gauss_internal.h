#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

void gauss_legendre_tbl(size_t n, double *x, double *w, double eps);
