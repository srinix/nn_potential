#include <stdio.h>
#include <stdlib.h>
#include "const.h"
#include "n3lo.h"

extern void __idahon3lo_MOD_n3lo(void);
extern void __idahon3lo600_MOD_n3lo600(void);
extern void __cdbonnpot_MOD_cdbonn();

extern struct crdwrt
{
  int kread,kwrite,kpunch,kda[9];
} crdwrt_;

extern struct cpot
{
  double v[6],xmev,ymev;
} cpot_;

extern struct cstate
{
  int j;
  int heform,sing,trip,coup,endep;
  char label[4];
  
} cstate_;

extern struct cnn
{
  int inn;
} cnn_;


int N3LO_EM(int jj,int in,double ki,double kf, double V[6])
{
  double unit = NN * hbarc*hbarc*hbarc;

  cstate_.heform = 0;
  cstate_.sing = 1;
  cstate_.trip = 1;
  cstate_.coup = 1;
  crdwrt_.kread=5;
  crdwrt_.kwrite=6;
  cpot_.xmev=ki*hbarc;
  cpot_.ymev=kf*hbarc;
  cstate_.j=jj;
  cnn_.inn=in;
  
  __idahon3lo_MOD_n3lo();
  
  V[0] = unit * cpot_.v[0];
  V[1] = unit * cpot_.v[1];
  V[2] = unit * cpot_.v[3];
  V[3] = unit * cpot_.v[5];
  V[4] = unit * cpot_.v[4];
  V[5] = unit * cpot_.v[2];
  
  return 0;
}

int N3LO_EM_600(int jj,int in,double ki,double kf, double V[6])
{
  double unit = NN * hbarc*hbarc*hbarc;
  
  cstate_.heform = 0;
  cstate_.sing = 1;
  cstate_.trip = 1;
  cstate_.coup = 1;
  crdwrt_.kread=5;
  crdwrt_.kwrite=6;
  cpot_.xmev=ki*hbarc;
  cpot_.ymev=kf*hbarc;
  cstate_.j=jj;
  cnn_.inn=in;
  
  __idahon3lo600_MOD_n3lo600();
  
   V[0] = unit * cpot_.v[0];
   V[1] = unit * cpot_.v[1];
   V[2] = unit * cpot_.v[3];
   V[3] = unit * cpot_.v[5];
   V[4] = unit * cpot_.v[4];
   V[5] = unit * cpot_.v[2];
  
  return 0;
}

int CD_BONN(int jj,int in,double ki,double kf, double V[6])
{
  double unit = NN * hbarc*hbarc*hbarc;

  cstate_.heform = 0;
  cstate_.sing = 1;
  cstate_.trip = 1;
  cstate_.coup = 1;
  crdwrt_.kread=5;
  crdwrt_.kwrite=6;
  cpot_.xmev=ki*hbarc;
  cpot_.ymev=kf*hbarc;
  cstate_.j=jj;
  cnn_.inn=in;
  
  __cdbonnpot_MOD_cdbonn();
  
  V[0] = unit * cpot_.v[0];
  V[1] = unit * cpot_.v[1];
  V[2] = unit * cpot_.v[3];
  V[3] = unit * cpot_.v[5];
  V[4] = unit * cpot_.v[4];
  V[5] = unit * cpot_.v[2];
  
  return 0;
}


