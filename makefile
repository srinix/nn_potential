CC=gcc
FF=gfortran
CFLAGS=-std=c89 -march=native
OPT=
LDFLAGS=-march=native -lm -lgfortran
SOURCES=$(wildcard */*.c */*.f)
HEADERS=$(wildcard */*.h)
MAIN=$(wildcard *.c)
OUT=$(patsubst %.c,%.out,$(MAIN))

ALL: $(OBJECTS) $(FOBJECTS) $(MAIN)
	$(CC) $(CFLAGS) $(OPT) $(MAIN) $(SOURCES) $(LDFLAGS) -o $(OUT)
	rm -rf *.o */*.o *.mod */*.mod
