#include <stdio.h>
#include <stdlib.h>
#include "lib_io/lib_io.h"
#include "lib_gauss/gauss.h"
#include "lib_pot/n3lo.h"

int main(int argc, char **argv)
{
   
  FILE *param_file, *pot_file, *mesh_file;
  struct params *param_list;
  size_t nk, i, j;
  int em, jm, ch, itz;
  double kmin, kmax, *k, *wt;
  char pot_out[100], mesh_out[100];
  
  param_file  = fopen(argv[1], "r");
  param_list = param_list_create(param_file);
  fclose(param_file);

  nk = (size_t)param_list_lookup("K-GRID-SIZE", param_list);
  kmin = param_list_lookup("K-START", param_list);
  kmax = param_list_lookup("K-MAX", param_list);
  jm = (int)param_list_lookup("J", param_list);
  em = (int)param_list_lookup("CUT-OFF", param_list);
  ch = (int)param_list_lookup("CHANNEL", param_list);
  itz = (int)param_list_lookup("ISOSPIN", param_list);

  
  sprintf(pot_out, "VNN_N3LO_%d_J%d_CH%d_K%d_N%d.dat", em, jm, ch, (int)kmax, (int)nk);
  sprintf(mesh_out, "VNN_N3LO_%d_J%d_CH%d_K%d_N%d.mesh", em, jm, ch, (int)kmax, (int)nk);

  pot_file  = fopen(pot_out, "w");

  param_list_print(pot_file, param_list);
  param_list_destroy(param_list);

  fprintf(pot_file, "##     K                  K'             V(K,K')\n");	 
  
  k = malloc(nk * sizeof(double));
  wt = malloc(nk * sizeof(double));
  gauss_grid_create(nk, k, wt, kmin, kmax);

  for (i = 0; i < nk; i++)
    {
      for (j = 0; j < nk; j++)
        {
          double Vtmp[6];
          if (em == 500)
            N3LO_EM(jm, itz, k[i], k[j], Vtmp);
          else if (em == 600)
            N3LO_EM_600(jm, itz, k[i], k[j], Vtmp);
	  else
            exit(-1);

	  if (ch < 2)
	    fprintf(pot_file, "%+.15E %+.15E %+.15E\n", k[i], k[j], Vtmp[ch]);
	  else 
	    fprintf(pot_file, "%+.15E %+.15E %+.15E %+.15E %+.15E %+.15E\n", k[i], k[j], Vtmp[2],
		    Vtmp[3], Vtmp[4], Vtmp[5]);
        }
      fprintf(pot_file, "\n");
    }

  fclose(pot_file);
  

  mesh_file = fopen(mesh_out, "w");

  for (i = 0; i < nk; i++)
    fprintf(mesh_file, "%+.15E %+.15E\n", k[i], wt[i]);

  fclose(mesh_file);

  free(k);
  free(wt);

  printf("POTENTIAL WRITTEN TO '%s' \nMESH WRITTEN TO '%s' \n", pot_out, mesh_out);
  
  return 0;
}
